<?php

namespace App\Http\Controllers\Client\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Client;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('client_guest')->except('logout');
    }

    //protected $redirectTo = '/';

    //Custom guard for seller
    protected function guard()
    {
        return Auth::guard('client');
    }



    //Shows seller login form
    public function showLoginForm()
    {

        return view('client.auth.login');
    }

    public function postlogin(Request $request)
    {

            $this->validate($request, [
            'user_name' => 'required',
            'password' => 'required',
        ]);

        $remember_me = $request->has('remember') ? true : false;
        $client = Client::where('user_name', $request->user_name)->first();

        if (!$client) {
            session()->flash('error_message', 'Username is not registered with us');
            return redirect('/client/login')->with('failed_message','Invalid Username !');

        } else if (Auth::guard('client')->attempt(['user_name' => $request->user_name, 'password' => $request->password], $remember_me)) {
            $this->guard()->login($client);
            return redirect('/client/home');
        } else {
            session()->flash('error_message', 'Incorrect Password');
            return redirect('/client/login')->with('failed_message','Password wrong !');;

        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/')->with('flash_message', 'Logout Successfully !');
    }
}
